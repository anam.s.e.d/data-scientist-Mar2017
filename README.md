Hiring for the Competitive Cities team, World Bank.

Deadline of application: April 10, 2017

Anticipated Start Date: The week of April 24, 2017

Please contact me (Anh Le - ale3@worldbank.org) for the full TOR with instructions of where to send your application.

As a **mandatory** part of the application, please complete a take-home task designed for us to understand your proficiency with data analysis and communication. All materials can be found in this respository, see file “Instructions.md”. Please click on `Repository > Files` to see.


